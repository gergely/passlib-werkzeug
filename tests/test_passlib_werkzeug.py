from passlib.registry import get_crypt_handler
from werkzeug.security import check_password_hash, generate_password_hash

from passlib_werkzeug import __version__


def test_version():
    assert __version__ == '0.1.0'


def test_werkzeug_understands_our_pbkdf2_sha256():
    handler = get_crypt_handler('werkzeug_pbkdf2_sha256')
    passlib_hash = handler.using(rounds=150000, salt_size=8).hash('password')

    assert check_password_hash(passlib_hash, 'password')


def test_we_understand_werkzeug_pbkdf2_sha256():
    handler = get_crypt_handler('werkzeug_pbkdf2_sha256')
    werkzeug_hash = generate_password_hash('password')

    assert handler.verify('password', werkzeug_hash)
